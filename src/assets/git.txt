1.-Creamos el repositorio GIT (local)

$ git init

//creamos .gitignore
$ nano .gitignore

$ git status //ver el estado de los archivos deberia de mostrar que los archivos aun faltan agregar

$ git add -A //Agregar todo los archivos

$ git commit -m “Inicializacion de versionamiento del nuevo repositorio”

$ git status
# On branch master
nothing to commit (working directory clean)
2.-Enlazamos el git remoto de bitbucket (Remoto)

$ git remote add origin https://<MI_USUARIO>@bitbucket.org/<MI_PROYECTO>/<REPOSITORIO>.git

$ git remote -v //Podremos ver las url a donde están apuntando

$ cat .git/config //Otra forma de ver la configuración

Subimos los archivos al git remote

$ git push -u origin master

3.-Subiendo un cambio de local a remoto

Primero guardamos los cambios en local

$ git status
$ git add -A
$ git commit -m “se guarda los cambios”
$ git status

4.- Subimos los cambios al bitbucket

$ git push origin master

5.-Bajando los cambios (otro usuario)

$ git pull origin master

6.-Bajando los cambios (todos los branch’s)

$ git pull

7.-Eliminar branch en el repositorio remoto

$ git push –delete  origin <branch_name>