import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor(private http: HttpClient) {
    // console.log("hola mundo")
  }

  getQuery(query: string) {
    const url = `https://api.spotify.com/v1/${query}`;

    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQB0W4npUmC9OhkS0Fg_lRYQskCltIeec2YtxHVq3bGf2qC43MIivjxyuqOFj81OJE1FW5bkgC4hGUrf-CM'
    });

    return this.http.get(url, { headers });

  }

  getNewReleases() {


    return this.getQuery('browse/new-releases?limit=20')
      .pipe(map(data => data['albums'].items));
    // return this.http.get('https://api.spotify.com/v1/browse/new-releases?limit=20', { headers })

  }

  getArtistas(termino: string) {

    return this.getQuery(`search?q=${termino}&type=artist&limit=15`)
      .pipe(map(data => data['artists'].items));
    // return this.http.get(`https://api.spotify.com/v1/search?q=${termino}&type=artist&limit=15`, { headers })

  }

  getArtista(id: string) {

    return this.getQuery(`artists/${id}`);
    // .pipe(map(data => data['artists'].items));
    // return this.http.get(`https://api.spotify.com/v1/search?q=${termino}&type=artist&limit=15`, { headers })

  }

  getTopTracks(id: string) {
    return this.getQuery(`artists/${id}/top-tracks?country=us`)
      .pipe(map(data => data['tracks']));
  }

}
